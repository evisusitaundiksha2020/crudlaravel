<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/mahasiswa','MahasiswaController@index')->name('index');
// route::resource('mahasiswa', 'MahasiswaController');

Route::get('/', [App\Http\Controllers\MahasiswaController::class, 'index']);
Route::get('/tambah', [App\Http\Controllers\MahasiswaController::class, 'tambah']);
Route::post('/store', [App\Http\Controllers\MahasiswaController::class, 'store']);
Route::get('/edit/{id}', [App\Http\Controllers\MahasiswaController::class, 'edit']);
Route::post('/update', [App\Http\Controllers\MahasiswaController::class, 'update']);
Route::get('/hapus/{id}', [App\Http\Controllers\MahasiswaController::class, 'hapus']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
