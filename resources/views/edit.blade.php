<!DOCTYPE html>
<html>
<head>
	<title>Update Data</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	@foreach($mahasiswa as $p)

	<form action="/update" method="post">
		{{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $p->id }}"> <br/>
        Nama <input type="text" required="required" name="nama" value="{{ $p->nama_mahasiswa }}"> <br/>
		Nim <input type="number" required="required" name="nim" value="{{ $p->nim_mahasiswa }}"> <br/>
		Kelas <input type="text" required="required" name="kelas" value="{{ $p->kelas_mahasiswa }}"> <br/>
		Prodi <input type="text" required="required" name="prodi" value="{{ $p->prodi_mahasiswa }}"> <br/>
        Fakultas <input type="text" required="required" name="fakultas" value="{{ $p->fakultas_mahasiswa }}"> <br/>
		<input type="submit" value="Simpan Data">
	</form>
	@endforeach
</body>
</html>